package pl.powiadamiacz.wsiz;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	// Context context; // ZMIANA
	final Context context = this;
	private List<Komunikat> mKomunikat = new ArrayList<Komunikat>();
	ProgressDialog dialog;
	KomunikatViewAdapter KomunikatViewAdapter;
	Button nowykomunikat;
	String regid;

	private TextView powitanie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// context = getApplicationContext(); // ZMIANA

		powitanie = (TextView) findViewById(R.id.powitanie);
		nowykomunikat = (Button) findViewById(R.id.nowykomunikat);

		if (checkPlayServices()) {
			// jest Play Services
			Log.i(Vars.TAG, "1");

			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);
			Log.i(Vars.TAG, "RegID: " + regid);
			Log.i(Vars.TAG, "Imie i nazwisko: " + getImieNazwisko(context));
			Log.i(Vars.TAG, "Stanowisko: " + getStanowisko(context));

			powitanie.setText("Witaj: " + getImieNazwisko(context) + ".");

			if (regid.isEmpty()) {
				// Nie ma ID rejestracji - uruchomienie Activity z rejestracja
				Intent intent = new Intent(MainActivity.this,
						RegisterActivity.class);

				startActivityForResult(intent, 100);
			} else {
				// Rejestracja ju� wykonana, a wi�c pobranie komunikat�w
				GetNoweKomunikaty("true"); // Wywo�anie funkcji do pobierania
											// aktualnych komunikat�w
			}

		} else {
			Log.i(Vars.TAG, "Brak Google Play Services!");
		}

		// Inicjalizacja przycisku nowykomunikat
		nowykomunikat.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg1) {

				// Dodanie �ledzenia Google Analytics
				EasyTracker easyTracker = EasyTracker.getInstance(context);
				easyTracker.send(MapBuilder.createEvent("Pobieranie danych",
						"Nowe Komunikaty",
						"Wykonanie funkcji do pobierania nowych komunikat�w",
						null).build());

				// Sprawdzenie czy u�ytkownik jest zarejestrowany
				regid = getRegistrationId(context);
				if (regid.isEmpty()) {
					// Nie ma ID rejestracji wi�� komunikat
					msg("Nie zarejstrowany nie mo�e dodawa� nowych komunikat�w. Zarejestruj si�.");
				} else {

					// Sprawdzamy po��czenie z Internetem
					if (CheckInternetConnection()) {
						// Po��czenie z Internetem jest, a wi�c wy�wietlamy
						// dialog
						LayoutInflater layoutInflater = LayoutInflater
								.from(context);
						View promptView = layoutInflater.inflate(
								R.layout.dialog_nowykom, null);
						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								context);
						alertDialogBuilder.setView(promptView);

						final EditText tresc = (EditText) promptView
								.findViewById(R.id.tresc);
						final CheckBox czyniezarejstrowany = (CheckBox) promptView
								.findViewById(R.id.czyniezarejstrowany);

						alertDialogBuilder
								.setCancelable(false)
								.setTitle("Wysy�anie nowego komunikatu")
								.setPositiveButton("Wy�lij",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												if (tresc.length() <= 3) {
													msg("Tre�� jest za kr�tka.");
												} else {
													if (czyniezarejstrowany
															.isChecked()) {
														new WyslijKomunikaty()
																.execute(
																		Vars.ApiUrl,
																		tresc.getText()
																				.toString(),
																		getImieNazwisko(context),
																		"TRUE");
													} else {
														new WyslijKomunikaty()
																.execute(
																		Vars.ApiUrl,
																		tresc.getText()
																				.toString(),
																		getImieNazwisko(context),
																		"FALSE");
													}
												}
											}
										})
								.setNegativeButton("Anuluj",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});

						// pokazanie dialogu
						AlertDialog alertD = alertDialogBuilder.create();

						alertD.show();

					} else {
						// Brak po��czenia a wi�c wy�wietlamy komunikat
						msg("Brak po��czenia z Internetem. Nie mo�esz wys�a� komunikatu.");
					}

				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices(); // sprawdzanie Google Play Services
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 100) {
			if (resultCode == RESULT_OK) {
				// zarejestrowano poprawnie
				powitanie.setText("Witaj: " + getImieNazwisko(context) + ".");

				Toast.makeText(getApplicationContext(),
						"Zarejestrowano poprawnie.", Toast.LENGTH_LONG).show();

				GetNoweKomunikaty("true"); // Wywo�anie funkcji do pobierania
											// aktualnych komunikat�w

			} else {
				powitanie.setText("Witaj: niezarejstrowana/y.");

				Toast.makeText(getApplicationContext(), "Nie zarejstrowano.",
						Toast.LENGTH_LONG).show();

				GetNoweKomunikaty("false"); // Wywo�anie funkcji do pobierania
											// aktualnych komunikat�w
			}
		}
	}

	/**
	 * Funkcja sprawdzaj�ca czy na urz�dzeniu jest zainstalowane Google Play
	 * Services
	 * 
	 * @return
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						Vars.PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(Vars.TAG, "Urz�dzenie nie jest obs�ugiwane!");
				finish();
			}
			return false;
		}
		return true;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(Vars.PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(Vars.TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Vars.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(Vars.TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	private String getImieNazwisko(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String imie_nazwisko = prefs.getString(Vars.PROPERTY_IMIE_NAZWISKO, "");
		if (imie_nazwisko.isEmpty()) {
			Log.i(Vars.TAG, "Nie znaleziono imienia i nazwiska");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Vars.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(Vars.TAG, "App version changed.");
			return "";
		}
		return imie_nazwisko;
	}

	private String getStanowisko(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String stanowisko = prefs.getString(Vars.PROPERTY_STANOWISKO, "");
		if (stanowisko.isEmpty()) {
			Log.i(Vars.TAG, "Nie znaleziono stanowiska");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(Vars.PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(Vars.TAG, "App version changed.");
			return "";
		}
		return stanowisko;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(RegisterActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Funkcja sprawdzaj�ca po�aczenie z Internetem
	 * 
	 * @return true w przypadku je�li jest, false - je�li nie ma
	 */
	private boolean CheckInternetConnection() {
		String networkService = Context.CONNECTIVITY_SERVICE;
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(networkService);
		if (connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnected()
				|| connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
						.isConnected()) {
			// Jest Polaczenie
			return true;
		} else {
			return false;
		}
	}

	private class Komunikat {
		public String data;
		public String tresc;
		public String autor;
	}

	private class KomunikatViewAdapter extends BaseAdapter {
		private LayoutInflater mInflater;

		public KomunikatViewAdapter() {
			mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return mKomunikat.size();
		}

		@Override
		public Komunikat getItem(int position) {
			return mKomunikat.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.listview_komunikaty,
						parent, false);

			} // Pobranie Obiektu Komunikat
			Komunikat komunikat = getItem(position);
			// Modyfikacja Zawarto�ci Pliku listview_komunikaty.xml
			TextView data = (TextView) convertView.findViewById(R.id.data);
			TextView tresc = (TextView) convertView.findViewById(R.id.tresc);
			TextView autor = (TextView) convertView.findViewById(R.id.autor);

			data.setText(komunikat.data);
			tresc.setText(komunikat.tresc);
			autor.setText(komunikat.autor);

			return convertView;
		}
	}

	/**
	 * Funkcja do pobierania danych json
	 * 
	 * @param linkapi
	 * @param czyzarejstrowany
	 */
	public void GetKomunikaty(String linkapi, Boolean czyzarejstrowany) {
		// PobieranieDanychJSON
		DefaultHttpClient httpclient = new DefaultHttpClient(
				new BasicHttpParams());
		HttpGet httpget;
		if (czyzarejstrowany) {
			// True a wi�c zarejestrowany
			httpget = new HttpGet(linkapi + "?task=getregister");
		} else {
			// Konto nie zarejestrowane
			httpget = new HttpGet(linkapi + "?task=getunregister");
		}
		httpget.setHeader("Content-type", "application/json");
		InputStream inputStream = null;
		String result = null;
		try {
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			inputStream = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					inputStream, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			result = sb.toString();
			Log.i(Vars.TAG, "Odp API: " + result);
			JSONObject jObject = new JSONObject(result);
			JSONArray jArray = jObject.getJSONArray("Komunikaty");
			if (jArray.length() == 0) {
				Komunikat komunikat = new Komunikat();
				// Przypisanie danych w obiekcie
				komunikat.tresc = "Brak komunikat�w";

				mKomunikat.add(komunikat);
			} else {

				for (int i = 0; i < jArray.length(); i++) {
					try {
						JSONObject oneObject = jArray.getJSONObject(i);
						// Przypisanie do zmiennych danych
						String data = oneObject.getString("data");
						String tresc = oneObject.getString("tresc");
						String autor = oneObject.getString("autor");

						Komunikat komunikat = new Komunikat();
						// Przypisanie danych w obiekcie
						komunikat.data = data;
						komunikat.tresc = tresc;
						komunikat.autor = autor;

						mKomunikat.add(komunikat);
					} catch (JSONException e) {
					}
				}
			}
		} catch (Exception e) {
			Log.i(Vars.TAG, "Exception: " + e);
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (Exception squish) {
			}
		}
	};

	/**
	 * Klasa do pobierania komunikat�w
	 * 
	 */
	private class Watek extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			// Czynno�ciprzed uruchomieniem w�tku
			mKomunikat.clear();// CzyszczenieListy
			dialog = new ProgressDialog(MainActivity.this);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setMessage("Pobieranie komunikat�w. Prosz� czeka�...");
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// G��wny w�tek
			// Pobranie komunikat�w

			if (params[0].equals("true")) {
				GetKomunikaty(Vars.ApiUrl, true);
			} else {
				GetKomunikaty(Vars.ApiUrl, false);
			}
			return "ok";
		}

		@Override
		protected void onPostExecute(String result) {
			// Tutaj czynno�ci po zako�czeniu w�tku
			dialog.dismiss();
			KomunikatViewAdapter = new KomunikatViewAdapter();
			ListView KomunikatyListView = (ListView) findViewById(R.id.komunikaty);
			KomunikatyListView.setAdapter(KomunikatViewAdapter);

			// Usuwanie po d�u�szym naci�ni�ciu - modearacja przez starost�
			KomunikatyListView
					.setOnItemLongClickListener(new OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> parent,
								View view, final int position, long id) {
							// Sprawdzamy czy u�ytkownik jest starost�
							if (!getStanowisko(context).equals("Starosta")) {
								msg("Komunikaty mo�e usuwa� tylko starosta.");
							} else {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										context);
								builder.setMessage(
										"Czy na pewno chcesz usun�� ten komunikat?")
										.setCancelable(false)
										.setPositiveButton(
												"Usu�",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														// Wywo�anie obiektu
														// usuwania komunikatu

														new UsunKomunikat()
																.execute(
																		Vars.ApiUrl,
																		mKomunikat
																				.get(position).data
																				.toString());
													}

												})
										.setNegativeButton(
												"Anuluj",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
														dialog.cancel();

													}
												});
								AlertDialog alert = builder.create();
								alert.show();
							}

							return false;
						}
					});
		}
	}

	/**
	 * Funkcja do pobierania nowych komunikat�w z API
	 * 
	 * @param czyzarejstrowany
	 *            true je�li u�ytkownik jest zarejestrowany, false je�li nie
	 */
	void GetNoweKomunikaty(String czyzarejstrowany) {

		// Dodanie �ledzenia Google Analytics
		EasyTracker easyTracker = EasyTracker.getInstance(this);
		easyTracker.send(MapBuilder.createEvent("Pobieranie danych",
				"Nowe Komunikaty",
				"Wykonanie funkcji do pobierania nowych komunikat�w", null)
				.build());

		if (CheckInternetConnection()) {
			// Po��czenie z Internetem jest, a wi�c pobieramy
			new Watek().execute(czyzarejstrowany);
		} else {
			// Brak po��czenia a wi�c wy�wietlamy komunikat
			msg("Brak po��czenia z Internetem. Nie mo�na pobra� komunikat�w.");
		}
	}

	/**
	 * Funkcja do wy�wietlania komunikatu w Toast
	 * 
	 * @param message
	 *            komunikat
	 */
	private void msg(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}

	/**
	 * Klasa do wys�ania komunikat�w
	 * 
	 */
	private class WyslijKomunikaty extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			// Przed uruchomieniem w�tku
			dialog = new ProgressDialog(MainActivity.this);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setMessage("Trwa wysy�anie. Prosz� czeka�...");
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// G��wny w�tek - wys�anie komunikatu do API
			String site = null;
			CharSequence uri = null;
			try {
				uri = params[0] + "?task=addmsg&tresc="
						+ URLEncoder.encode(params[1].toString(), "UTF-8")
						+ "&autor="
						+ URLEncoder.encode(params[2].toString(), "UTF-8")
						+ "&czy_niezarejstrowani_moga_widziec=" + params[3];
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				site = urlGet(String.valueOf(uri));
			} catch (IOException e) {
				Log.i(Vars.TAG, "Error wysy�ania komunikatu: " + e);
			}
			return site;
		}

		@Override
		protected void onPostExecute(String result) {
			// Po zako�czeniu w�tku

			Log.i(Vars.TAG, "Odp API z wysy�ania komunikatu: " + result);

			if (result.equals("ok")) {
				// Wys�ano komunikat
				msg("Komunikat zosta� wys�any!");
			} else {
				msg("Wyst�pi� b��d.");
			}
			dialog.dismiss();
			GetNoweKomunikaty("true"); // Od�wie�enie komunikat�w i pobranie
										// nowych
		}
	}

	/**
	 * Klasa do usuwania komunikat�w
	 * 
	 */
	private class UsunKomunikat extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			// Przed uruchomieniem w�tku
			dialog = new ProgressDialog(MainActivity.this);
			dialog.setIndeterminate(true);
			dialog.setCancelable(false);
			dialog.setMessage("Trwa usuwanie. Prosz� czeka�...");
			dialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// G��wny w�tek - wys�anie komunikatu do API
			String site = null;
			CharSequence uri = null;
			try {
				uri = params[0] + "?task=delmsg&data="
						+ URLEncoder.encode(params[1], "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				site = urlGet(String.valueOf(uri));
			} catch (IOException e) {
				Log.i(Vars.TAG, "Error usuwania komunikatu: " + e);
			}
			return site;
		}

		@Override
		protected void onPostExecute(String result) {
			// Po zako�czeniu w�tku
			if (result.equals("ok")) {
				// Usuni�to komunikat
				msg("Komunikat zosta� usuni�ty!");
			} else {
				msg("Wyst�pi� b��d.");
			}
			dialog.dismiss();
			GetNoweKomunikaty("true"); // Od�wie�enie komunikat�w i pobranie
										// nowych
		}
	}

	/**
	 * Funkcja do pobrania �r�d�a strony
	 * 
	 * @param urlString
	 * @return
	 * @throws IOException
	 */
	private String urlGet(String urlString) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response = httpclient.execute(new HttpGet(urlString));
		StatusLine statusLine = response.getStatusLine();
		if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			String responseString = out.toString();
			return responseString;
		} else {
			response.getEntity().getContent().close();
			throw new IOException(statusLine.getReasonPhrase());
		}
	}

}
