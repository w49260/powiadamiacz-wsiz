package pl.powiadamiacz.wsiz;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity {
	TextView mDisplay;
	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	Context context;
	ProgressDialog dialog;
	String regid;

	private EditText r_imienazwisko;
	private Spinner r_stanowisko;
	private Button zarejestruj;
	private TextView r_status;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);

		context = getApplicationContext();

		r_imienazwisko = (EditText) findViewById(R.id.r_imienazwisko);
		r_stanowisko = (Spinner) findViewById(R.id.r_stanowisko);
		zarejestruj = (Button) findViewById(R.id.zarejestruj);
		r_status = (TextView) findViewById(R.id.r_status);

		zarejestruj.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg1) {
				if (r_imienazwisko.length() < 5) {
					msg("Pole \"Imi� i nazwisko\" jest za kr�tkie. Popraw je.");
				} else {
					// Sprawdzamy po�aczenie z Internetem
					if (CheckInternetConnection()) {
						// Ukrywamy klawiature
						HideKeyboard();

						// Rejestrujemy
						registerInBackground();
					} else {
						msg("Brak po��czenia z Internetem.");
					}
				}
			}
		});
	}

	/**
	 * Funkcja sprawdzaj�ca po�aczenie z Internetem
	 * 
	 * @return true w przypadku je�li jest, false - je�li nie ma
	 */
	private boolean CheckInternetConnection() {
		String networkService = Context.CONNECTIVITY_SERVICE;
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(networkService);
		if (connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.isConnected()
				|| connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
						.isConnected()) {
			// Jest Polaczenie
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Funkcja ukrywaj�ca klawiatur�
	 */
	public void HideKeyboard() {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
				.getWindowToken(), 0);
	}

	/**
	 * Funkcja pobieraj�ca �r�d�o strony
	 * 
	 * @param urlString
	 *            - link do strony
	 * @return zwraca �r�d�o strony
	 * @throws IOException
	 */
	private String urlGet(String urlString) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response;

		try {
			response = httpclient.execute(new HttpGet(urlString));
		} catch (Exception e) {
			Log.i(Vars.TAG, "B��d pobierania strony: " + e);
			return "B��d";
		}

		StatusLine statusLine = response.getStatusLine();
		if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			String responseString = out.toString();
			return responseString;
		} else {
			response.getEntity().getContent().close();
			throw new IOException(statusLine.getReasonPhrase());
		}
	}

	/**
	 * Funkcja s�u��ca do wy�wietlenia komunikatu dla u�ytkownika
	 * 
	 * @param message
	 *            - wiadomo�� dla u�ytkownika
	 * 
	 */
	private void msg(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG)
				.show();
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(RegisterActivity.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Funkcja s�u��ca do rejestracji w GCM oraz API
	 * 
	 */
	private void registerInBackground() {
		new AsyncTask<Object, Object, Object>() {
			@Override
			protected void onPreExecute() {
				r_status.setText("Trwa rejestrowanie...");
			}

			@Override
			protected String doInBackground(Object... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					// Rejestracja w gcm
					regid = gcm.register(Vars.SENDER_ID);
					Log.i(Vars.TAG, regid);

					// Rejestracja na serwerze API
					String site = null;
					CharSequence uri = Vars.ApiUrl
							+ "?task=register&regid="
							+ URLEncoder.encode(regid, "UTF-8")
							+ "&imienazwisko="
							+ URLEncoder.encode(r_imienazwisko.getText()
									.toString(), "UTF-8")
							+ "&stanowisko="
							+ URLEncoder.encode(r_stanowisko.getSelectedItem()
									.toString(), "UTF-8");
					try {
						site = urlGet(String.valueOf(uri));
						Log.i(Vars.TAG, "Odpowied� serwera API: " + site);
					} catch (IOException e) {

					}
					if (site.equals("ok")) {
						// Poprawnie zarejestrowano

						// Zapisanie danych na urzadzeniu
						storeRegistrationId(context, regid, r_imienazwisko
								.getText().toString(), r_stanowisko
								.getSelectedItem().toString());

						msg = "ok";
					} else {
						msg = "B��d z rejestracj�. Spr�buj ponownie lub skontaktuj si� z autorem aplikacji.";
					}
				} catch (IOException ex) {
					Log.i(Vars.TAG, ex.getMessage());
					msg = "B��d: " + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Object result) {
				if (result.toString().contains("B��d")) {
					// Wystapi� b��d
					setResult(RESULT_CANCELED);

					if (result.toString().contains("SERVICE_NOT_AVAILABLE")) {
						// Us�uga nie jest dost�pna
						r_status.setText("B��d - us�uga GCM nie jest dost�pna.");
					} else {
						r_status.setText(result.toString());
					}
				} else {
					setResult(RESULT_OK);

					r_status.setText("Zarejestrowano");

					AlertDialog.Builder builder = new AlertDialog.Builder(
							RegisterActivity.this);
					builder.setMessage(
							"Twoje konto zosta�o zarejestrowane. Dzi�ki!")
							.setCancelable(false)
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// po naci�ni�ciu OK
											finish();
											dialog.cancel();
										}
									}).create().show();
				}
			}

		}.execute(null, null, null);
	}

	/**
	 * Funkcja do zapisywania ID rejstracji na urz�dzeniu!
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(Context context, String regId,
			String imienazwisko, String stanowisko) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(Vars.TAG, "Zapisano ustawienia dla wersji aplikacji:  "
				+ appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Vars.PROPERTY_REG_ID, regId);
		editor.putString(Vars.PROPERTY_IMIE_NAZWISKO, imienazwisko);
		editor.putString(Vars.PROPERTY_STANOWISKO, stanowisko);
		editor.putInt(Vars.PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

}
