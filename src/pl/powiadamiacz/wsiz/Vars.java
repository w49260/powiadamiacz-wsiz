package pl.powiadamiacz.wsiz;

public class Vars {
	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String PROPERTY_IMIE_NAZWISKO = "imie_nazwisko";
	public static final String PROPERTY_STANOWISKO = "stanowisko";

	public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	public static final String SENDER_ID = "495666300179";

	public static final String TAG = "GCMDemo";

	public static final String ApiUrl = "http://www.bramkapro.pl/wsiz/api.php";

}